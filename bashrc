alias ll='ls -l --color=auto'

SSH_ENV="$HOME/.ssh/environment"

# start the ssh-agent
function start_agent {
    echo "Initializing new SSH agent..."
    # spawn ssh-agent
    ssh-agent | sed 's/^echo/#echo/' > "$SSH_ENV"
    echo succeeded
    chmod 600 "$SSH_ENV"
    . "$SSH_ENV" > /dev/null
    ssh-add ~/.ssh/clickrain.id_rsa
    ssh-add ~/.ssh/personal.id_rsa
}

# test for identities
function test_identities {
    # test whether standard identities have been added to the agent already
    ssh-add -l | grep "The agent has no identities" > /dev/null
    if [ $? -eq 0 ]; then
        ssh-add
        # $SSH_AUTH_SOCK broken so we start a new proper agent
        if [ $? -eq 2 ];then
            start_agent
        fi
    fi
}

# check for running ssh-agent with proper $SSH_AGENT_PID
if [ -n "$SSH_AGENT_PID" ]; then
    ps -ef | grep "$SSH_AGENT_PID" | grep ssh-agent > /dev/null
    if [ $? -eq 0 ]; then
  test_identities
    fi
# if $SSH_AGENT_PID is not properly set, we might be able to load one from
# $SSH_ENV
else
    if [ -f "$SSH_ENV" ]; then
  . "$SSH_ENV" > /dev/null
    fi
    ps -ef | grep "$SSH_AGENT_PID" | grep ssh-agent > /dev/null
    if [ $? -eq 0 ]; then
        test_identities
    else
        start_agent
    fi
fi

PATH=$PATH:/c/wamp/bin/php/php5.4.3/:/c/wamp/bin/mysql/mysql5.5.24/bin/:/d/tools/spritemaker/:/d/tools/nimbus-tools/

alias stage='git update-ref refs/heads/staging refs/heads/dev && git checkout staging && git push && git checkout dev'
alias pushprod='git update-ref refs/heads/prod refs/heads/dev && git checkout prod && git push && git checkout dev'

# Our server doesn't recognize "cygwin" as a terminal with color, but it does recognize xterm.
# So, set our term to xterm, so that when I ssh in, it just works.
TERM=xterm

COLOR_RED="\e[0;31m"
COLOR_GREEN="\e[0;32m"
COLOR_YELLOW="\e[0;33m"
COLOR_CYAN="\e[0;36m"
COLOR_WHITE="\e[0;37m"
COLOR_GRAY="\e[1;30m"

# Git stuff.
is_git_repo() {
    $(git rev-parse --is-inside-work-tree &> /dev/null)
}

is_git_dir() {
    $(git rev-parse --is-inside-git-dir 2> /dev/null)
}

get_git_branch() {
    local branch_name

    # Get the short symbolic ref
    branch_name=$(git symbolic-ref --quiet --short HEAD 2> /dev/null) ||
    # If HEAD isn't a symbolic ref, get the short SHA
    branch_name=$(git rev-parse --short HEAD 2> /dev/null) ||
    # Otherwise, just give up
    branch_name="(unknown)"

    printf $branch_name
}

get_git_ahead_behind() {
    local ahead behind aheadbehind
    ahead=$(git status --short --branch | grep "##.*ahead" | sed -re "s/.*ahead ([0-9]+).*/\1/")
    behind=$(git status --short --branch | grep "##.*behind" | sed -re "s/.*behind ([0-9]+).*/\1/")
    if [[ -n $ahead ]]; then
    	ahead="+$ahead"
    fi
    if [[ -n $behind ]]; then
    	behind="-$behind"
    fi

    aheadbehind="${COLOR_GREEN}$ahead${COLOR_RED}$behind"

    echo $aheadbehind
}

# Git status information
prompt_git() {
    local git_info git_state uc us ut st

    if ! is_git_repo || is_git_dir; then
        return 1
    fi

    git_info="$(get_git_branch)${COLOR_WHITE}$(get_git_ahead_behind)"

    # Check for uncommitted changes in the index
    if ! $(git diff --quiet --ignore-submodules --cached); then
        uc="${COLOR_GREEN}!"
    fi

    # Check for unstaged changes
    if ! $(git diff-files --quiet --ignore-submodules --); then
        us="${COLOR_RED}!"
    fi

    # Check for untracked files
    if [ -n "$(git ls-files --others --exclude-standard)" ]; then
        ut="${COLOR_RED}+"
    fi

    # Check for stashed files
    if $(git rev-parse --verify refs/stash &>/dev/null); then
    	# Yellow $
        st="${COLOR_YELLOW}$"
    fi

    git_state=$uc$us$ut$st

    # Combine the branch name and state information
    if [[ $git_state ]]; then
        git_info="$git_info $git_state"
    fi

    printf " ${COLOR_CYAN}${git_info}"
}

PS1="${COLOR_WHITE}${COLOR_GREEN}\u@\h ${COLOR_YELLOW}\w\$(prompt_git)\[\033[0m\]\n$ "

